package dev.tacon.enums;

import java.util.EnumSet;
import java.util.Optional;

import dev.tacon.annotations.NonNull;

/**
 * Utility class to easily and safely manage enums.
 */
public final class EnumUtils {

	/**
	 * Looks for the enum constant of the specified enum class with the specified name.
	 * The name must match exactly an identifier used to declare an enum constant in this class.
	 * (Extraneous whitespace characters are not permitted.)
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the Class object of the enum class from which to return a constant
	 * @param name the name of the constant to return
	 * @return an {@code Optional} describing the corresponding enum constant, if found.
	 *         An empty {@code Optional} otherwise.
	 */
	public static <E extends Enum<E>> Optional<E> find(final @NonNull Class<E> enumClass, final @NonNull String name) {
		try {
			return Optional.of(Enum.valueOf(enumClass, name));
		} catch (final @SuppressWarnings("unused") IllegalArgumentException ex) {
			return Optional.empty();
		}
	}

	/**
	 * Checks if an {@code enum} has the value {@code name}
	 *
	 * @param <E>
	 * @param enumClass
	 * @param name name to test
	 * @return {@code true} if enumClass declares a {@code name} value, {@code false} otherwise.
	 */
	public static <E extends Enum<E>> boolean exists(final @NonNull Class<E> enumClass, final String name) {
		if (name != null) {
			try {
				Enum.valueOf(enumClass, name);
				return true;
			} catch (final @SuppressWarnings("unused") IllegalArgumentException ex) {
				// ignored
			}
		}
		return false;
	}

	/**
	 * Returns {@code null} if {@code name} is {@code null} otherwise
	 * the enum constant of the specified enum class with the
	 * specified name. The name must match exactly an identifier used
	 * to declare an enum constant in this class. (Extraneous whitespace
	 * characters are not permitted.)
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param name the name of the constant to return
	 * @return the enum constant of the specified enum class with the
	 *         specified name or {@code null}
	 * @throws IllegalArgumentException if the specified enum class has
	 *             no constant with the specified name, or the specified
	 *             class object does not represent an enum class
	 */
	public static <E extends Enum<E>> E valueOf(final @NonNull Class<E> enumClass, final String name) {
		return name == null ? (E) null : Enum.valueOf(enumClass, name);
	}

	/**
	 * Returns the enum constant of the specified enum class with the
	 * specified name if exists otherwise {@code null}.
	 * The name must match exactly an identifier used
	 * to declare an enum constant in this class. (Extraneous whitespace
	 * characters are not permitted.)
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param name the name of the constant to return
	 * @return the enum constant of the specified enum class with the
	 *         specified name or {@code null}
	 */
	public static <E extends Enum<E>> E safeValueOf(final @NonNull Class<E> enumClass, final String name) {
		return safeValueOf(enumClass, name, (E) null);
	}

	/**
	 * Returns the enum constant of the specified enum class with the
	 * specified name if exists otherwise a default value.
	 * The name must match exactly an identifier used
	 * to declare an enum constant in this class. (Extraneous whitespace
	 * characters are not permitted.)
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param name the name of the constant to return
	 * @param defaultValue the default value
	 * @return the enum constant of the specified enum class with the
	 *         specified name or {@code defaultValue}
	 */
	public static <E extends Enum<E>> E safeValueOf(final @NonNull Class<E> enumClass, final String name, final E defaultValue) {
		if (name != null) {
			try {
				return Enum.valueOf(enumClass, name);
			} catch (final @SuppressWarnings("unused") IllegalArgumentException ex) {
				// ignored
			}
		}
		return defaultValue;
	}

	/**
	 * Returns the enum constant with the specified key.
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param key key to search
	 * @return the enum constant with specified key
	 * @throws IllegalArgumentException if the key is not found
	 */
	public static <E extends Enum<E> & EnumWithKey<T>, T> E valueOfKey(final @NonNull Class<E> enumClass, final T key) {
		if (key == null) {
			return null;
		}
		for (final E constant : enumClass.getEnumConstants()) {
			if (key.equals(constant.key())) {
				return constant;
			}
		}
		throw new IllegalArgumentException("No enum constant found with key = " + key);
	}

	/**
	 * Returns the enum constant with the specified key or {@code null}
	 * if no enum constant exist.
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param key key to search
	 * @return the enum constant with specified key or {@code null}
	 */
	public static <E extends Enum<E> & EnumWithKey<T>, T> E safeValueOfKey(final @NonNull Class<E> enumClass, final T key) {
		return safeValueOfKey(enumClass, key, (E) null);
	}

	/**
	 * Returns the enum constant with the specified key or a default value
	 * if no enum constant is found.
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param key key to search
	 * @return the enum constant with specified key or {@code defaultValue}
	 */
	public static <E extends Enum<E> & EnumWithKey<T>, T> E safeValueOfKey(final @NonNull Class<E> enumClass, final T key, final E defaultValue) {
		if (key != null) {
			for (final E constant : enumClass.getEnumConstants()) {
				if (key.equals(constant.key())) {
					return constant;
				}
			}
		}
		return defaultValue;
	}

	/**
	 * Returns the enum constant with the specified key case-insensitive.
	 *
	 * @param <T> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param key key to search
	 * @return the enum constant with specified key or {@code null}
	 * @throws IllegalArgumentException if the key is not found
	 */
	public static <E extends Enum<E> & EnumWithKey<String>> E valueOfKeyIgnoreCase(final @NonNull Class<E> enumClass, final String key) {
		if (key == null) {
			return null;
		}
		for (final E constant : enumClass.getEnumConstants()) {
			if (key.equalsIgnoreCase(constant.key())) {
				return constant;
			}
		}
		throw new IllegalArgumentException("No enum constant found with key = " + key);
	}

	/**
	 * Returns the enum constant with the specified key case-insensitive or {@code null}
	 * if no enum constant is found.
	 *
	 * @param <T> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param key key to search
	 * @return the enum constant with specified key or {@code null}
	 */
	public static <E extends Enum<E> & EnumWithKey<String>> E safeValueOfKeyIgnoreCase(final @NonNull Class<E> enumClass, final String key) {
		return safeValueOfKeyIgnoreCase(enumClass, key, (E) null);
	}

	/**
	 * Returns the enum constant with the specified key case-insensitive or a default value
	 * if no enum constant is found.
	 *
	 * @param <T> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param key key to search
	 * @return the enum constant with specified key or {@code defaultValue}
	 */
	public static <E extends Enum<E> & EnumWithKey<String>> E safeValueOfKeyIgnoreCase(final @NonNull Class<E> enumClass, final String key, final E defaultValue) {
		if (key != null) {
			for (final E constant : enumClass.getEnumConstants()) {
				if (key.equalsIgnoreCase(constant.key())) {
					return constant;
				}
			}
		}
		return defaultValue;
	}

	/**
	 * Returns the enum constant with the specified code.
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param code code to search
	 * @return the enum constant with specified code
	 * @throws IllegalArgumentException if the code is not found
	 */
	public static <E extends Enum<E> & EnumWithCode> E valueOfCode(final @NonNull Class<E> enumClass, final int code) {
		for (final E constant : enumClass.getEnumConstants()) {
			if (code == constant.code()) {
				return constant;
			}
		}
		throw new IllegalArgumentException("No enum constant found with code = " + code);
	}

	/**
	 * Returns the enum constant with the specified code or {@code null}
	 * if no enum constant is found.
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param code code to search
	 * @return the enum constant with specified code or {@code null}
	 */
	public static <E extends Enum<E> & EnumWithCode> E safeValueOfCode(final @NonNull Class<E> enumClass, final int code) {
		return safeValueOfCode(enumClass, code, (E) null);
	}

	/**
	 * Returns the enum constant with the specified code or a default value
	 * if no enum constant is found.
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param code code to search
	 * @return the enum constant with specified code or {@code defaultValue}
	 */
	public static <E extends Enum<E> & EnumWithCode> E safeValueOfCode(final @NonNull Class<E> enumClass, final int code, final E defaultValue) {
		for (final E constant : enumClass.getEnumConstants()) {
			if (code == constant.code()) {
				return constant;
			}
		}
		return defaultValue;
	}

	/**
	 * Returns the enum constant with the specified code.
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param code code to search
	 * @return the enum constant with specified code
	 * @throws IllegalArgumentException if the code is not found
	 */
	public static <E extends Enum<E> & EnumWithChar> E valueOfCode(final @NonNull Class<E> enumClass, final char code) {
		for (final E constant : enumClass.getEnumConstants()) {
			if (code == constant.code()) {
				return constant;
			}
		}
		throw new IllegalArgumentException("No enum constant found with code = " + code);
	}

	/**
	 * Returns the enum constant with the specified code or {@code null}
	 * if no enum constant is found.
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param code code to search
	 * @return the enum constant with specified code or {@code null}
	 */
	public static <E extends Enum<E> & EnumWithChar> E safeValueOfCode(final @NonNull Class<E> enumClass, final char code) {
		return safeValueOfCode(enumClass, code, (E) null);
	}

	/**
	 * Returns the enum constant with the specified code or a default value
	 * if no enum constant is found.
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param code code to search
	 * @return the enum constant with specified code or {@code defaultValue}
	 */
	public static <E extends Enum<E> & EnumWithChar> E safeValueOfCode(final @NonNull Class<E> enumClass, final char code, final E defaultValue) {
		for (final E constant : enumClass.getEnumConstants()) {
			if (code == constant.code()) {
				return constant;
			}
		}
		return defaultValue;
	}

	/**
	 * Returns the enum constant with the specified code.
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param code code to search or {@code null}.
	 * @return the enum constant with specified code or {@code null} if {@code code} is null.
	 * @throws IllegalArgumentException if the code is not found
	 */
	public static <E extends Enum<E> & EnumWithCode> E valueOfCode(final @NonNull Class<E> enumClass, final Integer code) {
		if (code == null) {
			return null;
		}
		return valueOfCode(enumClass, code.intValue());
	}

	/**
	 * Returns the enum constant with the specified code.
	 *
	 * @param <E> The enum class whose constant is to be returned
	 * @param enumClass the {@code Class} object of the enum class from which
	 *            to return a constant
	 * @param code code to search or {@code null}.
	 * @return the enum constant with specified code or {@code null} if {@code code} is null.
	 * @throws IllegalArgumentException if the code is not found
	 */
	public static <E extends Enum<E> & EnumWithChar> E valueOfCode(final @NonNull Class<E> enumClass, final Character code) {
		if (code == null) {
			return null;
		}
		return valueOfCode(enumClass, code.charValue());
	}

	/**
	 * Returns the name of this enum constant, exactly as declared in its enum declaration.
	 *
	 * @param <E> The enum class
	 * @param constant the enum constant
	 * @return the name of this enum constant or {@code null} if the specified constant is null.
	 */
	public static <E extends Enum<E>> String name(final E constant) {
		return name(constant, null);
	}

	/**
	 * Returns the name of this enum constant (exactly as declared in its enum declaration),
	 * if present, or a default description.
	 *
	 * @param <E> The enum class
	 * @param constant the enum constant
	 * @return the name of this enum constant or {@code defaultValue} if the specified constant is null.
	 */
	public static <E extends Enum<E>> String name(final E constant, final String defaultValue) {
		return constant == null ? defaultValue : constant.name();
	}

	/**
	 * Returns the key of this enum constant, if present, or null.
	 *
	 * @param <E> The enum class
	 * @param constant the enum constant
	 * @return the key of this enum constant or {@code null} if the specified constant is null.
	 */
	public static <T, E extends Enum<E> & EnumWithKey<T>> T key(final E constant) {
		return key(constant, (T) null);
	}

	/**
	 * Returns the key of this enum constant, if present, or a default description.
	 *
	 * @param <E> The enum class
	 * @param constant the enum constant
	 * @return the key of this enum constant or {@code defaultValue} if the specified constant is null.
	 */
	public static <T, E extends Enum<E> & EnumWithKey<T>> T key(final E constant, final T defaultValue) {
		return constant == null ? defaultValue : constant.key();
	}

	/**
	 * Returns the code of this enum constant, if present, or null.
	 *
	 * @param <E> The enum class
	 * @param constant the enum constant
	 * @return the code of this enum constant or {@code null} if the specified constant is null.
	 */
	public static <E extends Enum<E> & EnumWithCode> Integer code(final E constant) {
		return constant == null ? null : Integer.valueOf(constant.code());
	}

	/**
	 * Returns the code of this enum constant, if present, or null.
	 *
	 * @param <E> The enum class
	 * @param constant the enum constant
	 * @return the code of this enum constant or {@code null} if the specified constant is null.
	 */
	public static <E extends Enum<E> & EnumWithChar> Character charCode(final E constant) {
		return constant == null ? null : Character.valueOf(constant.code());
	}

	/**
	 * Returns the description of this enum constant, if present, or null.
	 *
	 * @param <E> The enum class
	 * @param constant the enum constant
	 * @return the description of this enum constant or {@code null} if the specified constant is null.
	 */
	public static <E extends Enum<E> & EnumWithDescription> String description(final E constant) {
		return description(constant, null);
	}

	/**
	 * Returns the description of this enum constant, if present, or a default description.
	 *
	 * @param <E> The enum class
	 * @param constant the enum constant
	 * @return the description of this enum constant or {@code defaultValue} if the specified constant is null.
	 */
	public static <E extends Enum<E> & EnumWithDescription> String description(final E constant, final String defaultValue) {
		return constant == null ? defaultValue : constant.description();
	}

	/**
	 * Creates an enum set initially containing the specified elements.
	 * This factory, whose parameter list uses the varargs feature, may
	 * be used to create an enum set initially containing an arbitrary
	 * number of elements, but it is likely to run slower than the overloadings
	 * that do not use varargs.
	 *
	 * @param <E> The class of the parameter elements and of the set
	 * @param first an element that the set is to contain initially
	 * @param rest the remaining elements the set is to contain initially
	 * @throws NullPointerException if any of the specified elements are null,
	 *             or if {@code rest} is null
	 * @return an enum set initially containing the specified elements
	 */

	/**
	 * Creates an enum set initially containing the specified elements.
	 *
	 * @param <E> The class of the parameter elements and of the set
	 * @param enumClass the enum class
	 * @param constants the elements the set is to contain initially
	 * @throws NullPointerException if any of the specified elements are null,
	 *             or any parameter is null
	 * @return an enum set initially containing the specified elements
	 */
	@SafeVarargs
	@NonNull
	public static <E extends Enum<E>> EnumSet<E> enumSet(final @NonNull Class<E> enumClass, final @NonNull E... constants) {
		return constants.length > 0 ? EnumSet.of(constants[0], constants) : EnumSet.noneOf(enumClass);
	}

	private EnumUtils() {
		throw new AssertionError();
	}
}
