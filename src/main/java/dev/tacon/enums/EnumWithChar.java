package dev.tacon.enums;

/**
 *
 * {@code interface} for an {@code enum} with a unique {@code char} code
 *
 */
public interface EnumWithChar {

	/**
	 * Gets the code for this constant
	 */
	char code();
}
