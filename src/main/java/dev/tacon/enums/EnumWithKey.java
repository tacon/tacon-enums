package dev.tacon.enums;

import dev.tacon.annotations.NonNullByDefault;

/**
 *
 * {@code interface} for an {@code enum} with a unique key of type {@code T}
 *
 * @param <T>
 */
@NonNullByDefault
public interface EnumWithKey<T> {

	/**
	 * Gets the key for this constant
	 */
	T key();
}