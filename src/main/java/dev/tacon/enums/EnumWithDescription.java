package dev.tacon.enums;

import java.util.Locale;

import dev.tacon.annotations.NonNullByDefault;

/**
 *
 * {@code interface} for an {@code enum} with a description
 *
 */
@NonNullByDefault
public interface EnumWithDescription {

	/**
	 * Gets a description for this constant
	 */
	String description();

	/**
	 * Gets a description for this constant in the Locale
	 *
	 * @param locale the locale to use, not null
	 */
	default String description(final Locale locale) {
		return this.description();
	}
}
