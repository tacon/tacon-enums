package dev.tacon.enums;

/**
 *
 * {@code interface} for an {@code enum} with a unique {@code int} code
 *
 */
public interface EnumWithCode {

	/**
	 * Gets the code for this constant
	 */
	int code();
}